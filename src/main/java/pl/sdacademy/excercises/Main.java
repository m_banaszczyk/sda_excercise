package pl.sdacademy.excercises;

public class Main {

    public static void main(String[] args) {
        final Dog dog = new Dog();
        //TypObiektu nazwaObiektu = wywołanie konstruktora Typu (konstruktor ze wszystkimi parametrami
        final Dog pimpek = new Dog("Pimpek");
        pimpek.getName();
        pimpek.setName("Pimpuś");
    }
}
