package pl.sdacademy.excercises;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter  //linijka 24-26
@Setter// linijka 28-30
@AllArgsConstructor //linijka 16-18
@NoArgsConstructor //linijka 20-22

public class Dog {

    private String name;

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public Dog(String name) {
//        this.name = name;
//    }
//    public Dog(){
//        this("bezimienny");
//    }
}
